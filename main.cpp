
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>

extern char **environ;

int print_Status(int status)
{
    if(WIFEXITED(status) != 0)
    {
        printf("Child procces end after exit() with status: %d\n", WEXITSTATUS(status));
        return EXIT_SUCCESS;
    }
}


int execvpe(char * filename, char*argv[], char * environment[])
{
    char ** prev_environment = environ;
    environ = environment;
    int excepv_result = execvp(filename,argv);
    environ = prev_environment;
    return excepv_result;
}

void print_Environment()
{
    for(int i = 0; environ[i] != NULL; i++)
    {
        printf("%s\n", environ[i]);
    }
}

char * check_envp[] = {"PATH=/home/students/2019/m.kigel/oc/lab11\"", "environ2","environ3","environ4",NULL};

int main(int argc, char ** argv)
{
    int status;
    pid_t genProcces;
    if(argc < 2)
    {
        perror("you don't put the argument");
        return EXIT_FAILURE;
    }
    printf("Parent's environ: ");
    print_Environment();

    genProcces = fork();
    if(genProcces == -1)
    {
        perror("Can't create a child");
        return  EXIT_FAILURE;
    }
    int execpveResult;
    if(genProcces == 0)
    {
        execpveResult = execvpe(argv[1], &argv[1], check_envp);
        if(execpveResult == -1)
        {
            perror("Error in function exec");
            return EXIT_FAILURE;
        }
        printf("Child's environ: ");
        print_Environment();
    }

    int waitResult = wait(&status);
    if(waitResult == -1)
    {
        perror("Error in function wait");
        return  EXIT_FAILURE;
    }
    print_Status(status);
    return EXIT_SUCCESS;
}
